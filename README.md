# ST-LINK_V2

### 介绍

集成st-link&虚拟串口的工具

### 1.Hardware

1.  ST-Link.SchDoc
2.  ST-Link.PcbDoc

### 2.Firmware

1.  STLINKV2.J31M21.bin

### 3.Tools

​	1.st-linkv2_upgrade

​	2.st-linkv2_usbdriver

​	3.STM32 ST-LINK Utility v4.3.0 setup

​	4.固件烧录说明.txt

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
